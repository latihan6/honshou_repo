<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <button><a href="/servletMVC/LoginController">Back</a></button>
	<form action="CreateController" method="post">
		Enter Name : <input type="text" name="name"> <BR>
		Enter Email : <input type="email" name="email"> <BR>
		Enter Password : <input type="password" name="password"> <BR>
		Enter address : <input type="text" name="address"> <BR>
		Enter status : <select name="status">
		                    <option selected disabled>Choose</option>
		                    <option value="Active">Active</option>
		                    <option value="Non Active">Non Active</option>
		                    <option value="On Leave">On Leave</option>
		               </select>
		<BR>
		Enter physics : <input type="number" name="physics"> <BR>
		Enter calculus : <input type="number" name="calculus"> <BR>
		Enter biologi : <input type="number" name="biologi"> <BR>
		<input type="submit" value="Save"/>
	</form>
</body>
</html>
