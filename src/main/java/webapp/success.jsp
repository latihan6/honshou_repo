<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>Insert title here</title>
</head>
<body>
    <h1>Data</h1>
    <button><a href="CreateController">Add Data</a></button>
    <table border="1">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>Status</th>
                <th>Physics</th>
                <th>Calculus</th>
                <th>Biologi</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach var="element" items="${requestScope['data']}">
            <tr>
                <td>${element.fullname}</td>
                <td>${element.email}</td>
                <td>${element.address}</td>
                <td>${element.status}</td>
                <td>${element.physics}</td>
                <td>${element.calculus}</td>
                <td>${element.biologi}</td>
                <td>
                    <button><a href="/servletMVC/edit?id=${element.id}">Edit</a></button>
                    <button><a href="/servletMVC/delete?id=${element.id}">Delete</a></button>
                </td>
            </tr>
         </c:forEach>
        </tbody>
    </table>
</body>
</html>
