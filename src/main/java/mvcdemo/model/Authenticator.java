package mvcdemo.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.Pattern;

public class Authenticator {
    public String authenticate(String username, String password) {
        String v = "";
        Koneksi conn = new Koneksi();
        Connection c = conn.Koneksi();
        boolean muser = Pattern.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+|\\+62\\d*", username);
        boolean mpass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,100}$", password);
        try {
            Statement statement = c.createStatement();

            if (muser && mpass) {
                ResultSet res = statement.executeQuery("SELECT * FROM user WHERE email='"+username+"' AND password='"+password+"'");
                res.next();
                String nama = res.getString("fullname");
                if (nama == null || nama == "") {
                    v = "failure";
                } else {
                    v = "success";
                }
            } else {
                v = "failure";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }
}
