package mvcdemo.model;
public class User {
    private int id;
    private String email;
    private String password;
    private String fullname;
    private String address;
    private String status;
    private int physics;
    private int calculus;
    private int biologi;

    public User(int id_user, String fullname, String email, String password, String address, String status, int physics, int calculus, int biologi) {
        this.id = id_user;
        this.fullname = fullname;
        this.email = email;
        this.password = password;
        this.address = address;
        this.status = status;
        this.physics = physics;
        this.calculus = calculus;
        this.biologi = biologi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPhysics() {
        return physics;
    }

    public void setPhysics(int physics) {
        this.physics = physics;
    }

    public int getCalculus() {
        return calculus;
    }

    public void setCalculus(int calculus) {
        this.calculus = calculus;
    }

    public int getBiologi() {
        return biologi;
    }

    public void setBiologi(int biologi) {
        this.biologi = biologi;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String username) {
        this.email = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
