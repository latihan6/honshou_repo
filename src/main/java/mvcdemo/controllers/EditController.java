package mvcdemo.controllers;

import mvcdemo.model.Koneksi;
import mvcdemo.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class EditController extends HttpServlet {
    protected Koneksi conn = new Koneksi();
    private Connection c = conn.Koneksi();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
        Statement statement = c.createStatement();
        String id_user = req.getParameter("id");
        PrintWriter writer = res.getWriter();
        ResultSet result = statement.executeQuery("SELECT * FROM user WHERE id_user='"+id_user+"'");
        result.next();
        User user = new User(result.getInt("id_user"),result.getString("fullname"), result.getString("email"), result.getString("password"), result.getString("address"), result.getString("status"), result.getInt("physics"), result.getInt("calculus"), result.getInt("biologi"));
        RequestDispatcher rd = req.getRequestDispatcher("/edit.jsp");
        req.setAttribute("data", user);
        rd.forward(req, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Koneksi conn = new Koneksi();
        Connection c = conn.Koneksi();
        int id = Integer.parseInt(req.getParameter("id"));
        String fullname = req.getParameter("name");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String address = req.getParameter("address");
        String status = req.getParameter("status");
        int physics = Integer.parseInt(req.getParameter("physics"));
        int calculus = Integer.parseInt(req.getParameter("calculus"));
        int biologi = Integer.parseInt(req.getParameter("biologi"));
        try {
            Statement statement = c.createStatement();
            statement.execute("UPDATE user SET fullname='"+fullname+"',email='"+email+"',password='"+password+"',address='"+address+"',status='"+status+"',physics='"+physics+"',calculus='"+calculus+"',biologi='"+biologi+"' WHERE id_user='"+id+"'");
            res.sendRedirect("/servletMVC/LoginController");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
