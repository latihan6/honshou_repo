package mvcdemo.controllers;

import mvcdemo.model.Koneksi;
import mvcdemo.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.regex.Pattern;

public class CreateController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RequestDispatcher rd = null;
        rd = req.getRequestDispatcher("/create.jsp");
        rd.forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Koneksi conn = new Koneksi();
        Connection c = conn.Koneksi();
        String fullname = req.getParameter("name");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String address = req.getParameter("address");
        String status = req.getParameter("status");
        int physics = Integer.parseInt(req.getParameter("physics"));
        int calculus = Integer.parseInt(req.getParameter("calculus"));
        int biologi = Integer.parseInt(req.getParameter("biologi"));
        boolean mpass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,100}$", password);
        if (mpass) {
            try {
                Statement statement = c.createStatement();
                statement.execute("INSERT INTO user(fullname,email,password,address,status,physics,calculus,biologi) VALUES('"+fullname+"','"+email+"','"+password+"','"+address+"','"+status+"','"+physics+"','"+calculus+"','"+biologi+"')");
                res.sendRedirect("/servletMVC/LoginController");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            RequestDispatcher rd = req.getRequestDispatcher("/servletMVC/badReq.jsp");
            rd.forward(req, res);
        }
    }
}
